import { Router } from 'express';
import { KelompokAccountService } from './../services';
const router = Router();

router.get("/",(req,res) => {
  KelompokAccountService
    .findAllKelompokAccount()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      logger.debug('Debug statement');
      console.log("Error ocured");
      console.log(err);
      console.error("error")
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    });

});
router.get("/:id",(req,res) => {
  KelompokAccountService
    .findKelompokAccountById(req.params.id)
    .then(function(data) {
      res.status(200).json({
        status: 200,
        data: data
      });
    }).catch(function(error) {
      res.status(400)
        .json({
          status: 400,
          message : error
        })
    });
});
module.exports = router;
