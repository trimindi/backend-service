import { Router } from 'express';
import { JenisAccountService } from './../services';
const router = Router();

router.get("/",(req,res) => {
  JenisAccountService
    .findAllJenisAccount()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      console.error(err);
      res.status(400)
        .json({
          status: 400,
          message : err
        })
    })

});
router.get("/:id",(req,res) => {
  JenisAccountService
    .findJenisAccountById(req.params.id)
    .then(function(data) {
      res.status(200).json({
        status: 200,
        data: data
      });
    }).catch(function(error) {
      res.status(400)
        .json({
          status: 400,
          message : error
        })
    });
});
module.exports = router;
