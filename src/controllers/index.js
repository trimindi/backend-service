
import AccountController from './AccountController';
import AuthController from './AuthController';
import BukuBesarController from './BukuBesarController';
import JenisAccountController from './JenisAccountController';
import KelompokAccountController from './KelompokAccountController';
import AnggotaController from './AnggotaController';
import MasterController from './MasterController';
module.exports = {
  AccountController : AccountController,
  AuthController : AuthController,
  BukuBesarController : BukuBesarController,
  JenisAccountController : JenisAccountController,
  KelompokAccountController : KelompokAccountController,
  AnggotaController : AnggotaController,
  MasterController: MasterController
}
