import { Router } from 'express';
import { AccountService } from './../services';

const router = Router();

router.get("/",(req,res) => {
  AccountService
    .findAllAccount(req.decoded.cif)
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err
        })
    })

});

router.get("/:id",(req,res) => {
  AccountService
    .findAccountById(req.params.id)
    .then(function(data) {
      res.status(200).json({
        status: 200,
        data: data
      });
    }).catch(function(error) {
      res.status(400)
        .json({
          status: 400,
          message : error
        })
    });
});
module.exports = router;
