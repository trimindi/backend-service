import { Router } from 'express';
import { MasterService } from './../services';

const router = Router();

router.get("/pendidikan",(req,res) => {
  MasterService
    .findAllPendidikan()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});
router.get("/pekerjaan",(req,res) => {
  MasterService
    .findAllJenisPekerjaan()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});

router.get("/golongan",(req,res) => {
  MasterService
    .findAllGolongan()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});

router.get("/identitas",(req,res) => {
  MasterService
    .findAllJenisIdentitas()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});
router.get("/jabatan",(req,res) => {
  MasterService
    .findAllJabatan()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});
router.get("/pendidikan",(req,res) => {
  MasterService
    .findAllPendidikan()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});
router.get("/agama",(req,res) => {
  MasterService
    .findAllAgama()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});
router.get("/hub_keluarga",(req,res) => {
  MasterService
    .findAllHubunganKeluarga()
    .then(data => {
      res.status(200).json({
        status: 200,
        data: data
      });
    })
    .catch(err => {
      res.status(400)
        .json({
          status: 400,
          message : err.message
        })
    })

});

module.exports = router;
