import { Router } from 'express';
import { UserService } from './../services';
import md5 from 'md5';
import jwt from 'jsonwebtoken';
import moment from 'moment';
const router = Router();


router.post("/login", (req,res) => {
  UserService
    .findByUsername(req.body.username)
    .then(data =>  {
      if (md5(req.body.password) == data.password) {
          UserService.findRoleUsers(data.id)
            .then(roles => {
              let payload = {
                  id: data.id,
                  cif: data.cif,
                  username: data.username,
                  first_name: data.first_name,
                  last_name: data.last_name,
                  fule: roles.map(v => v.role),
                  issued_at: moment(new Date()).format(),
                  expired_at : moment(new Date()).add(3, 'hours').format()
              }
              let token = jwt.sign(payload, 'sukrigantengsekali', {
                  expiresIn: '3h'
              });
              res.status(200).json(Object.assign(payload, {
                  status: 200,
                  token: token
              }));
            }).catch(err => {
                res.status(400).json({
                  status: 400,
                  message : err.message
                });
            })

      } else {
          res.status(401).json({
              status: 401,
              msg: "Username / Password anda Salah"
          });
      }
    }).catch(function(error) {
      res.status(401)
        .json({
          status: 401,
          message : "Username / Password Tidak Valid"
        })
    });
});
module.exports = router;
