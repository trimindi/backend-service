import promise from 'bluebird';
let options = {
	promiseLib: promise
};
const pgp = require('pg-promise')(options);
const connectionString = 'postgres://postgres:1234@localhost:5432/kpri';
const db = pgp(connectionString);

module.exports = db;
