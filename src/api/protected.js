import { version } from '../../package.json';
import { Router } from 'express';
import {
  AccountController,
  BukuBesarController,
  JenisAccountController,
  KelompokAccountController,
  AnggotaController,
  MasterController
} from './../controllers';
export default () => {
	let api = Router();
	api.use('/coa/account', AccountController);
  api.use('/coa/bukubesar', BukuBesarController);
  api.use('/coa/jenis', JenisAccountController);
  api.use('/coa/kelompok', KelompokAccountController);
  api.use('/anggota',AnggotaController);
  api.use('/master',MasterController)
	return api;
}
