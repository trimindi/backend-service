import db from './../db';

const findAllGolongan = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_golongan where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};

const findAllPendidikan = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_pendidikan where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};

const findAllAgama = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_agama where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};
const findAllHubunganKeluarga = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_hub_keluarga where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};
const findAllJenisIdentitas = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_identitas where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};

const findAllJenisPekerjaan = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_pekerjaan where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};

const findAllJabatan = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from dt_jabatan where is_deleted = false order by id asc")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};




module.exports = {
  findAllGolongan: findAllGolongan,
  findAllPendidikan: findAllPendidikan,
  findAllAgama: findAllAgama,
  findAllHubunganKeluarga: findAllHubunganKeluarga,
  findAllJenisIdentitas: findAllJenisIdentitas,
  findAllJabatan: findAllJabatan
}
