import db from './../db';
import md5 from 'md5';
import moment from 'moment';
const findByUsername = (username) => {
    return new Promise((resolve , reject) => {
      db.one("select * from users where username = $1 and is_deleted = false",[username])
      .then(data => {
          resolve(data);
      })
      .catch(err => {
          reject(err);
      });
    })

};
const findRoleUsers = (id) => {
    return new Promise((resolve , reject) => {
      let sql = `SELECT role from users_roles LEFT JOIN roles on users_roles.role_id = roles.id WHERE users_roles.user_id = $1`;
      db.many(sql,[id])
      .then(data => {
          resolve(data);
      })
      .catch(err => {
          reject(err);
      });
    })

};

const updateUsers = (old ,payload) => {
    return new Promise((resolve , reject) => {
      let query = `
        UPDATE users SET
          username = $1,
          password = $2 ,
          first_name = $3,
          last_name = $4,
          email = $5,
          phone_number = $6,
          alamat = $7,
          update_at = $8
          WHERE
          id = $9
      `;
      db.query(query, [
        payload.username || old.username,
        (payload.password ? md5(payload.password) : null)  || old.password,
        payload.first_name || old.first_name,
        payload.last_name || old.last_name,
        payload.email || old.email,
        payload.phone_number || old.phone_number,
        payload.alamat || old.alamat,
        moment(Date.now()).format(),
        old.id
      ]).then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
}


module.exports = {
  findByUsername: findByUsername,
  updateUsers: updateUsers,
  findRoleUsers: findRoleUsers
}
