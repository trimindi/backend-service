import db from './../db';

const findAllAccount = (cif) => {
   return new Promise((resolve,reject) => {
     db.many("SELECT * FROM mst_account where is_deleted = false and cif = $1",[cif])
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });
};
const findAccountById = (id) => {
    return new Promise((resolve,reject) => {
        db.one("SELECT * FROM mst_account where is_deleted = false and id = $1",[id])
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        });
    });
};


module.exports = {
  findAllAccount: findAllAccount,
  findAccountById: findAccountById
}
