import db from './../db';

const findAllAccountBukuBesar = (cif) => {
   return new Promise((resolve,reject) => {
     db.any("SELECT * FROM mst_account_buku_besar where is_deleted = false and cif = $1",[cif])
       .then(data => {
          resolve(data);
       })
       .catch(err => {
         console.log(err);
          reject(err);
       });
   });

};
const findAccountBukuBesarById = (id) => {
    return new Promise((resolve,reject) => {
        db.one("SELECT * FROM mst_account_buku_besar where is_deleted = false and id = $1",[id])
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        });
    });
};


module.exports = {
  findAllAccountBukuBesar: findAllAccountBukuBesar,
  findAccountBukuBesarById: findAccountBukuBesarById
}
