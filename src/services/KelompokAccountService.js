import db from './../db';

var findAllKelompokAccount = () => {
   return new Promise((resolve,reject) => {
     db.many("SELECT * FROM mst_account_kelompok")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          console.log(err);
          reject(err);
       });
   });

};
var findKelompokAccountById = (id) => {
    return new Promise((resolve,reject) => {
        db.one("SELECT * FROM mst_account_kelompok where is_deleted = false and id = $1",[id])
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        });
    });
};


module.exports = {
  findAllKelompokAccount: findAllKelompokAccount,
  findKelompokAccountById: findKelompokAccountById
}
