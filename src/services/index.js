import userService from './UserService';
import accountService from './AccountService';
import anggotaService from './AnggotaService';
import accountBukuBesarService from './AccountBukuBesarService';
import jenisAccountService from './JenisAccountService';
import kelompokAccountService from './KelompokAccountService';
import masterService from './MasterService';
module.exports = {
  UserService: userService,
  AccountService: accountService,
  AccountBukuBesarService: accountBukuBesarService,
  AnggotaService: anggotaService,
  JenisAccountService : jenisAccountService,
  KelompokAccountService: kelompokAccountService,
  MasterService: masterService
}
