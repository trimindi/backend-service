import db from './../db';

const findAllJenisAccount = () => {
   return new Promise((resolve,reject) => {
     db.many("select * from mst_account_jenis")
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });

};
const findJenisAccountById = (id) => {
    return new Promise((resolve,reject) => {
        db.one("SELECT * FROM mst_account_jenis where is_deleted = false and id = $1",[id])
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        });
    });
};


module.exports = {
  findAllJenisAccount: findAllJenisAccount,
  findJenisAccountById: findJenisAccountById
}
