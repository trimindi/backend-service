import db from './../db';

const findAllAnggota = (cif) => {
   return new Promise((resolve,reject) => {
     db.many("SELECT * FROM mst_anggota where is_deleted = false and cif = $1",[cif])
       .then(data => {
          resolve(data);
       })
       .catch(err => {
          reject(err);
       });
   });

};
const findAnggotaById = (id) => {
    return new Promise((resolve,reject) => {
        db.one("SELECT * FROM mst_anggota where is_deleted = false and id = $1",[id])
        .then(data => {
            resolve(data);
        })
        .catch(err => {
            reject(err);
        });
    });
};


module.exports = {
  findAnggotaById: findAnggotaById,
  findAllAnggota: findAllAnggota
}
